<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="robots" content="noindex">
    <meta name="googlebot" content="noindex">

    <title>Check In</title>
    
    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/blog-home.css" rel="stylesheet">
    <link href="css/logo.css" rel="stylesheet">
    
    <!-- jQuery -->
    <script src="js/jquery.js"></script>
    <script src = "js/time.js"></script>

</head>

<style>
body, html {
    height: 100%;
    background-repeat: no-repeat;
/*    background-image: linear-gradient(rgb(104, 145, 162), rgb(12, 97, 33));*/
}
   .main {
       border-radius: 25px;
       background-color: #FFF;
   }
   option {
       width: 125px;
   }


</style>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container main" >
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            <a href="./" class="logo"><img src="img/SalviGroup-Logo_100x53.png" /></a>
            </div>
  
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="./admin/">Admin</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->
    <div class="container main">

        <div class="row">

            <!-- Blog Entries Column -->
            <div class="col-md-8">

                <h1 class="page-header">
                    Visitor
                    <small>Sign-In</small>
                </h1>
                <h1>
                    <a href="#">The time is</a>
                        <p class="glyphicon glyphicon-time"></p>
                        <span  class="text-center" id="time">
                            <!--<p  id="time"></p>-->
                        </span>   
                        <span id="timee"></span>
                </h1>
 

<div class="container ">
  <!--<h2>Vertical (basic) form</h2>-->
  <form role="form" method="POST" action="insert.php">
      <div class="form-group col-md-2"></div>
    <div class="form-group col-md-4">
        <label for="first_name">First Name</label>
        <input type="first_name" class="form-control text-center" id="first_name" required="yes" name="first_name" placeholder="First Name">
        <label for="last_name">Last Name</label>
          <input type="last_name" class="form-control text-center" id="last_name" required="yes" name="last_name" placeholder="Last Name">
        <label for="company_name">Company Name</label>
          <input type="company_name" class="form-control text-center" id="company_name" required="yes" name="company_name" placeholder="Company Name">
          
          
          <!--uncomment this section to require a pin to sign out-->
          <!--<label for="pin">Temporary Pin:</label>
          <input type="password" class="form-control text-center" max="4" id="password" required="yes" name="pin" maxlength="4" placeholder="Temporary Pin">-->
        <br>
    <button type="submit" class="btn btn-success btn-lg btn-block col-lg-12" name="signin">Sign In</button>
    <button type="submit" class="btn btn-danger btn-lg btn-block col-lg-12" name="signout">Sign Out</button>
    </div>
    <div class="form-group col-md-4">

    </div>  
    

  </form>
</div>
    
                <p class="lead"></p>
            </div>

            <!-- Blog Sidebar Widgets Column -->
            <div class="col-md-4">
                <br>
                
 
<?php
        //Connect to database         
	require_once('inc/connect-db.php');
                
        // get results from database
//        require_once('inc/data.php');
        
        // display data in table
        while($row = mysql_fetch_array( $result )) {
?>                                                                                       
 

<?php } ?>              
                                
                            

<!-- Side Widget Well 1 -->
<form action="insert.php">
    <div class="well">
        <h4>User Sign-In</h4>
        <div class="row">
            <div class="col-lg-12">
                <table class="table table-striped ">
                  <tr>
                  </tr>
<?php

        // get results from database
        require_once('inc/activein.php');
        

?>                                                                                       

<td>                                
    <lable>Select a user to sign in</lable>
    <select name='first_name' style="margin-top:10px;" class="btn-block btn-lg">

        <option value='select one'>Select User</option>
<?php        
    // display data in table
    while($row = mysql_fetch_array( $result )) {  
    echo "<option value='" .$row['ID']. "'>" .$row['first_name']. " " .$row['last_name'] ."</option>";
    }
    ?>
    </select>
                            </td>
                            </table>
                        </div>
                        <!-- /.col-lg-6 -->

                    <!-- /.row -->
                </div>
            </div>
</form>
                
                
                
                <!-- Side Widget Well 2 -->
               <div class="well">
                    <h4>User Sign-out</h4>
                    <div class="row">
                        <div class="col-lg-12">
                            <table class="table table-striped ">
                              <tr>
                              </tr>
<?php

        // get results from database
        require_once('inc/selectusr.php');
        

?>                                                                                       

<td>                                
    <lable>Select a user to sign out</lable>
    <select name='first_name' style="margin-top:10px;" class="btn-block btn-lg">

        <option value='select one'>Select User</option>
<?php        
    // display data in table
    while($row = mysql_fetch_array( $result )) {  
    echo "<option value='".$row['ID']."'>" .$row['first_name']. " " .$row['last_name'] ."</option>";
    }
    ?>
    </select>
                            </td>
                            </table>
                        </div>
                        <!-- /.col-lg-6 -->

                    <!-- /.row -->
                </div>

            </div>
                
                
                <!-- Side Widget Well 3 -->
               <div class="well">
                    <h4>Visitors Onsite</h4>
                    <div class="row">
                        <div class="col-lg-12">
                            <table class="table table-striped ">
                              <tr>
<!--                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Company</th>-->
                              </tr>
<?php

                
        // get results from database
        require_once('inc/count.php');
        
        // display data in table
        while($row = mysql_fetch_array( $result )) {
?>                                                                                       
<!--                            <ul class="list-unstyled">-->
                                    <tr >
                                        <td><lable>Visitors Currently Signed In</lable></td> 
                                        <td><lable><?php echo $row['count']; ?></lable></td>                             
                                    </tr>

<?php } ?>              
                                </table>
                        </div>
                        <!-- /.col-lg-6 -->

                    <!-- /.row -->
                </div>

            </div>

        </div>
        <!-- /.row -->

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; Salvi Group 2015</p>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </footer>

    </div>
    <!-- /.container -->

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    <script>
    $(document).ready(function(){
        $.ajax({
        type: "POST",
        url: "insert.php",
        data: {'function': 'load'},
        dataType: "json",
        async: false,
        success: function (data) {
            result = data;
            dataObject = jQuery.parseJSON(result);
        }
});
    }
    
    
    });
    </script>
</body>

</html>
