<?php

//error_reporting(E_ALL);
//ini_set("display_errors", 1);

//-- Set timezone to MDT --//
define('TIMEZONE', 'America/Denver');
date_default_timezone_set('America/Denver');

//-- Function to Remove All Non-Numeric Characters --//
function keepNumbers($input){
	//echo $input.'<br /><br />';
	$input = preg_replace("/[^0-9]+/", "", $input);
	//echo $input.'<br />';
	return $input;
}
//-- Function to Truncate value length to specific size --//
function truncateLength($input, $len){
	$input = strval($input);
	$input = substr($input, 0, $len);
	return $input;
}

function cleanInput($input) {
 
  $search = array(
    '@<script[^>]*?>.*?</script>@si',   // Strip out javascript
    '@<[\/\!]*?[^<>]*?>@si',            // Strip out HTML tags
    '@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly
    '@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments
  );
 
    $output = preg_replace($search, '', $input);
    return $output;
  }
  
function logoutById($id,$con,$login_number){
          $is_signin = '0';
       $sign_in_day = date("d/m/Y");
       $sign_in = date_create()->format('Y-m-d H:i:s');
        $login_number_table = $login_number;
        $login_number++;
        
// -- Insert info into database -- //
$sql='UPDATE ckin_user SET `sign_out'.$login_number_table.'` = "'.$sign_in.'",  `is_signin` = "0", `login_number` = "'.$login_number.'" WHERE `ID` = "'.$ID.'" AND `date` = "'.$sign_in_day.'";';
if (!mysqli_query($con,$sql))
  {
  die('Error: ' . mysqli_error($con));
  }
}
  

function insert_login($login_number,$first_name,$last_name,$company,$con){
    $sign_in_day = date("d/m/Y");
       $sign_in = date_create()->format('Y-m-d H:i:s');
// -- Insert info into database -- //
$sql='UPDATE ckin_user SET `sign_in'.$login_number.'` = "'.$sign_in.'",  `is_signin` = "1", `login_number` = "'.$login_number.'" WHERE `first_name` = "'.$first_name.'" AND `last_name` = "'.$last_name.'" AND `company_name` = "'.$company.'" AND `date` = "'.$sign_in_day.'";';

if (!mysqli_query($con,$sql))
  {
  die('Error: ' . mysqli_error($con));
  }
}
function insert_logout($login_number,$first_name,$last_name,$company,$con){
       $is_signin = '0';
       $sign_in_day = date("d/m/Y");
       $sign_in = date_create()->format('Y-m-d H:i:s');
        $login_number_table = $login_number;
        $login_number++;
        
// -- Insert info into database -- //
$sql='UPDATE ckin_user SET `sign_out'.$login_number_table.'` = "'.$sign_in.'",  `is_signin` = "0", `login_number` = "'.$login_number.'" WHERE `first_name` = "'.$first_name.'" AND `last_name` = "'.$last_name.'" AND `company_name` = "'.$company.'" AND `date` = "'.$sign_in_day.'";';
if (!mysqli_query($con,$sql))
  {
  die('Error: ' . mysqli_error($con));
  }
}
 function first_login($first_name,$last_name,$company,$pin,$ip_address,$con){
    $is_signin = '1';
    
    $sign_in_day = date("d/m/Y");
    $sign_in1 = date_create()->format('Y-m-d H:i:s');
// -- Insert info into database -- //
$sql="INSERT INTO ckin_user (first_name, last_name, company_name, sign_in1, is_signin, pin, ip_address, login_number, date)
VALUES
	('$first_name','$last_name','$company', '$sign_in1','$is_signin','$pin', '$ip_address', '1', '$sign_in_day')";
if (!mysqli_query($con,$sql))
  {
  die('Error: ' . mysqli_error($con));
  }
}


$con=mysqli_connect("localhost","Nick","password","checkin");
// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }
//First Name field
//  $first_name = .mysqli_real_escape_string($_REQUEST['first_name']);
//  $first = cleanInput(mysql_real_escape_string($_POST['first_name']), 16);
//  $first_name = preg_replace("/[^0-9]+/", "", $_POST['first_name']);
$first_name = preg_replace('#[^A-Za-z0-9]#i','', $_POST["first_name"]);

$first_name = mysqli_real_escape_string($con, ucfirst($first_name));

  $last_name = preg_replace('#[^A-Za-z0-9]#i','', $_POST["last_name"]);
  $last_name = mysqli_real_escape_string($con, ucfirst($last_name));

//  $company = preg_replace('#[^A-Za-z0-9]#i','', $_POST["company_name"]);
//  $company = preg_replace("/[^a-z0-9_\s-]/", '', $_POST['company_name']);
//  $company = mysqli_real_escape_string($con, ucwords($company));
$company = $_POST['company_name'];  
$company = strtolower($company); 
$company = ucwords($company);
$company = str_replace('<', '', $company);
$company = mysqli_real_escape_string($con, $company);

$pin = md5(filter_input(INPUT_POST, 'pin'));
$ip_address = $_SERVER['REMOTE_ADDR'];
     
      $sign_in_day = date("d/m/Y");
      
     $sql="SELECT * FROM ckin_user WHERE `first_name` = '".$first_name."' AND `last_name` = '".$last_name."' AND `company_name` = '".$company."' AND `date` = '".$sign_in_day."';";
  
      $results = mysqli_query($con,$sql);
      $row = $results->fetch_array(MYSQLI_ASSOC);
      
if($row === null){
    first_login($first_name,$last_name,$company,$pin,$ip_address,$con);
}elseif($row['is_signin'] == '1'){
         if(isset($_POST['signin'])){
             var_dump('You are already signed in');
         }elseif(isset($_POST['signout'])){
             insert_logout($row['login_number'],$first_name,$last_name,$company,$con);
         }
}elseif($row['is_signin'] == '0'){
         if(isset($_POST['signin'])){
             if($row['login_number'] === null){
             }else{
                insert_login($row['login_number'],$first_name,$last_name,$company,$con);
             }
         }elseif(isset($_POST['signout'])){
             var_dump('You are already signed out');
         }
     }

if(isset($_POST['ID'])){
         var_dump('in the if');
     die;
    logoutById($_POST['ID'], $con, $row['login_number']);
}

     
     
//AJAX CALL TO GET PEOPLE WHO HAVE LOGGED IN TODAY     
$function = $_POST['function'];
if($function == 'load'){
    
    $sql="SELECT first_name, last_name, company_name FROM ckin_user WHERE `date` = '".$sign_in_day."';";
    $results = mysqli_query($con,$sql);
      $row = $results->fetch_array(MYSQLI_ASSOC);
      
      echo json_encode($row);
}

     
     
header('Location: ./');

mysqli_close($con);
 ?>
