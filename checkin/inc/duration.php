<?php

// calculate time duration
$start_date = new DateTime($row['sign_out1']);
$since_start = $start_date->diff(new DateTime($row['sign_in1']));
$tdays = $since_start->days.' days total<br>';
$years = $since_start->y.' years<br>';
$months = $since_start->m.' months<br>';
$days = $since_start->d.' days<br>';
$hours = $since_start->h.' hours<br>';
$minutes = $since_start->i.' minutes<br>';
$seconds = $since_start->s.' seconds<br>';