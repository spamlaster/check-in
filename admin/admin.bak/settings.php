
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Admin</title>
    
    <!-- Bootstrap Core CSS -->
    <link href="../../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../css/blog-home.css" rel="stylesheet">
    <link href="../../css/logo.css" rel="stylesheet">
    
</head>

<style>
body, html {
    /*height: 100%;*/
    background-repeat: no-repeat;
    /*background-image: linear-gradient(rgb(104, 145, 162), rgb(12, 97, 33));*/
}
   .main {
       border-radius: 25px;
       background-color: #FFF;
   }
   .page-header {
       margin: 20px 10px;
   }
</style>

<body>
<?php if($error) { 
	echo "<div>{$error}</div>";
	}
?>
    

   <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container main" >
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
<!--                <a class="navbar-brand" href="#">Company Name</a>-->
            <a href="../" class="logo"><img src="../../img/SalviGroup-Logo_100x53.png" /></a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="../admin/">Admin</a>
                    </li>
                    <li>
                        <a href="../admin/settings.php">Settings</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->
    <div class="container main">
    
        <div class="row">
            <h1 class="page-header">
                Admin
                <small>Settings</small>
            </h1>
                         

<div class="container table-responsive">
<table class="table table-striped table-pagination display" id="myTable">
    <thead>
    <tr>
      <th>ID</th>
      <th>User name</th>
      <th>Password</th>
      <th>Settings</th>
    </tr>
    </thead>
<?php
        //Connect to database         
	require_once('../../inc/connect-db.php');
                
        $query = 'SELECT * FROM login order by ID DESC;';
        $result = mysql_query($query)
        or die(mysql_error());  

        // display data in table
        while($row = mysql_fetch_array( $result )) {
            

?>                                                                                       
<!--                            <ul class="list-unstyled">-->
    <tr>
        <td><?php echo $row['ID']; ?> </td> 
        <td><?php echo $row['username']; ?> </td>                                         
        <td class="password" type="password"><?php echo $row['password']; ?> </td> 
        <td><a class="btn btn-primary" href="edit.php?ID=<?php echo $row['ID'] ?>"><span class="glyphicon glyphicon-search"> Edit</span></a></td>
    </tr>

<?php } ?>    

    
   
</table>                              
</div>




        <!-- /.row -->

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; Check In 2015</p>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </footer>
        </div>
    </div>
    

    <!-- /.container -->
    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    
</body>

</html>
