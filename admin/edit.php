<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Admin Settings   </title>
    
    <!-- Bootstrap Core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../css/blog-home.css" rel="stylesheet">
    <link href="../css/logo.css" rel="stylesheet">
   
</head>

<style>
body, html {
    /*height: 100%;*/
    background-repeat: no-repeat;
    /*background-image: linear-gradient(rgb(104, 145, 162), rgb(12, 97, 33));*/
}
   .main {
       border-radius: 25px;
       background-color: #FFF;
   }
   .page-header {
       margin: 20px 10px;
   }
</style>

<body>
    
    <!-- Navigation -->
 <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
     <div class="container main" >
         <!-- Brand and toggle get grouped for better mobile display -->
         <div class="navbar-header">
             <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                 <span class="sr-only">Toggle navigation</span>
                 <span class="icon-bar"></span>
                 <span class="icon-bar"></span>
                 <span class="icon-bar"></span>
             </button>
<!--                <a class="navbar-brand" href="#">Company Name</a>-->
         <a href="../" class="logo"><img src="../img/SalviGroup-Logo_100x53.png" /></a>
         </div>
         <!-- Collect the nav links, forms, and other content for toggling -->
         <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
             <ul class="nav navbar-nav">
                 <li>
                     <a href="../admin/">Admin</a>
                 </li>
                 <li>
                     <a href="../admin/settings.php">Settings</a>
                 </li>
             </ul>
         </div>
         <!-- /.navbar-collapse -->
     </div>
     <!-- /.container -->
 </nav>
    
<?php
include '../inc/connect-db.php';
        // get results from database
	$getcode = $_REQUEST["ID"];
	$query = 'SELECT * FROM login WHERE ID='.$getcode.';';
        $result = mysql_query($query)
                or die(mysql_error());  
                
        // display data in table
        while($row = mysql_fetch_array( $result )) {
            
            
?>        
<div class="container-fluid">
    <div class="row-fluid"> 
        <div class="span12">
            <div class="page-header">  
<h1><?php echo $row['username']; ?></h1><hr>	

 <!--echo out the contents of each row into a table-->
<form class='form-horizontal' role='form' action='update.php?ID=<?php echo $getcode; ?>' method='POST'>
    <div class="form-group">
        <label class="col-md-2 control-label">ID <span style="padding-left: 5px;"><?php echo $row['ID']; ?></span> </label>
    </div>
        <input type="hidden" name="ID" value="<?php  $row['ID']; ?>">
    <div class="form-group">
        <label class="col-md-2 control-label">Username</label>
        <div class="col-sm-2">
          <input type="text" name="username" value="<?php echo $row['username'];?>"
        </div></div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Password</label>
        <div class="col-sm-2">
            <input class="password" type="password" name="password" value="<?php echo $row['password']; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Roll</label>
        <div class="col-sm-2">
            <input type="text" name="roll" value="<?php $row['roll'] ?>"> 
        </div>
    </div>   
    <div class="form-group">
        <div class="col-lg-6">
            <button class="btn btn-lg btn-primary" type="submit">Update</button>
        </div>
    </div>  

            </div>
        </div>
    </div>
</div>

</form>
<?php } ?>