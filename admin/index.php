<?php include '../security.php'; ?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Admin</title>
    
    <!-- Bootstrap Core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../css/blog-home.css" rel="stylesheet">
    <link href="../css/logo.css" rel="stylesheet">
    <link href="../css/dataTables.bootstrap.css" real="stylesheet">
    <link rel="stylesheet" href="../css/jquery.dataTables.min.css">
    
    <script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
    <script type="text/javascript" src="./moment.js"></script>


<!-- jQuery -->
    <script src="../js/dataTables.bootstrap.js"></script>
<script>
/* Custom filtering function which will search data in column four between two values */
$.fn.dataTable.ext.search.push(
    function( settings, data, dataIndex ) {
        var min = moment($('#iStartDateCol').val(),  "DD/MM/YYYY" );
        var max = moment($('#iEndDateCol').val(),  "DD/MM/YYYY" );
        var age = moment(data[12],  "DD/MM/YYYY" ) || moment("01/01/1000",  "DD/MM/YYYY" ); // use data for the age column
        console.log(min);
        console.log(max);
 
        if ( ( !min.isValid() && !max.isValid() ) ||
             ( !min.isValid() && age <= max ) ||
             ( min <= age   && !max.isValid() ) ||
             ( min <= age   && age <= max ) )
        {
            return true;
        }
        return false;
    }
);

$(document).ready(function(){
    var table = $('#myTable').DataTable();
    //tete
    // Event listener to the two range filtering inputs to redraw on input
    $('#iStartDateCol, #iEndDateCol').keyup( function() {
        table.draw();
    } );
});
</script>

    
</head>

<style>
body, html {
    /*height: 100%;*/
    background-repeat: no-repeat;
    /*background-image: linear-gradient(rgb(104, 145, 162), rgb(12, 97, 33));*/
}
   .main {
       border-radius: 25px;
       background-color: #FFF;
   }
   .page-header {
       margin: 20px 10px;
   }
</style>

<body>
<?php if($error) { 
	echo "<div>{$error}</div>";
	}
?>
   

   <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container main" >
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
<!--                <a class="navbar-brand" href="#">Company Name</a>-->
            <a href="../" class="logo"><img src="../img/SalviGroup-Logo_100x53.png" /></a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="../admin/">Admin</a>
                    </li>
                    <li>
                        <a href="../admin/settings.php">Settings</a>
                    </li>
                </ul>
                <a class="nav navbar-header btn-danger btn-lg pull-right" style="margin-top: 5px;" href="logout.php">&nbsp;<i class="glyphicon glyphicon-off">&nbsp;LogOut</i></a>                
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->
    <div class="container main">
    
        <div class="row">
            <h1 class="page-header">
                Admin
                <small>All Stats</small>
            </h1>
                 
<!--<div class="span9 columns">
  <h2>Example</h2>
  <p>Attached to a field with the format specified via options.</p>
  <div class="well">
    <input type="text" class="span2" name="search_box" value="" id="dp1" placeholder="Start Date" >
    <input type="text" class="span2" name="search_end" value="" id="dp2" placeholder="End Date">
    <input type="submit" name="search" value="search the table">
  </div> -->

<div class="container table-responsive">
<table class="table table-striped table-pagination display" id="myTable">
    
    <div id="myTable_filter" class="dataTables_filter">
    <lable>Start Date:</lable>
    <input id="iStartDateCol" name="min" type="text">
    <lable>End Date:</lable>
    <input id="iEndDateCol" name="max" type="text">
    </div>
    <br>
    
    <thead>
    <tr>
      <th>ID</th>
      <th>First Name</th>
      <th>Last Name</th>
      <th>Company</th>
      <th>Sign In</th>
      <th>Sign Out</th>
      <th>Sign In 2</th>
      <th>Sign Out 2</th>
      <th>Sign In 3</th>
      <th>Sign Out 3</th>
      <th>Sign In 4</th>
      <th>Sign Out 4</th>
      <!-- <th>Login Count</th>           -->
      <!--      <th>Duration</th>      -->
<!--      <th>Is Signed In</th> -->
<!--      <th>User IP</th> 
      <th>Signout IP</th>  -->                              
      <th>Date Accessed</th>
    </tr>
    </thead>
<?php
        //Connect to database         
	require_once('../inc/connect-db.php');
                
        $query = 'SELECT * FROM ckin_user order by ID DESC;';
        $result = mysql_query($query)
        or die(mysql_error());  

        // display data in table
        while($row = mysql_fetch_array( $result )) {
            $sign_in = $row['sign_in1'] ? date("g:i a", strtotime($row['sign_in1'])) : '-';
            $sign_out = $row['sign_out1'] ? date("g:i a", strtotime($row['sign_out1'])) : '-';
            $sign_in2 = $row['sign_in2'] ? date("g:i a", strtotime($row['sign_in2'])) : '-';
            $sign_out2 = $row['sign_out2'] ? date("g:i a", strtotime($row['sign_out2'])) : '-'; 
            $sign_in3 = $row['sign_in3'] ? date("g:i a", strtotime($row['sign_in3'])) : '-';
            $sign_out3 = $row['sign_out3'] ? date("g:i a", strtotime($row['sign_out3'])) : '-';            
            $sign_in4 = $row['sign_in4'] ? date("g:i a", strtotime($row['sign_in4'])) : '-';
            $sign_out4 = $row['sign_out4'] ? date("g:i a", strtotime($row['sign_out4'])) : '-';  
            
            
if (isset($_POST["search"])) {
	
	$search_term = mysql_real_escape_string($_POST["search_box"]);	
	$search_end = mysql_real_escape_string($_POST["search_end"] . ' 23:59:59');	
	$sql .= " WHERE date BETWEEN '{$search_term}'";	
	$sql .= " AND '{$search_end}'";		

echo '<h3 style="color:#0000FF; text-shadow: 1px 1px #FFD700;">' . date('l F j, Y',strtotime($search_term)) . '<span style="color:#000;">' . ' - ' . '</span>';
echo date('l F j, Y',strtotime($search_end)) . '</h3>'; 

}
// calculate time duration
$start_date = new DateTime($row['sign_out1']);
$since_start = $start_date->diff(new DateTime($row['sing_in1']));
$tdays = $since_start->days.' days total<br>';
$since_start->y.' years<br>';
$since_start->m.' months<br>';
$days = $since_start->d.' days<br>';
$hours = $since_start->h.' hours<br>';
$minutes = $since_start->i.' minutes<br>';
$seconds = $since_start->s.' seconds<br>';

//if (empty($test)) {
//    $test = '-';
//}

?>                                                                                       

    <!--                            <ul class="list-unstyled">-->
    <tr id="<?php echo $row['ID']; ?>" class="tablerow">
        <td><?php echo $row['ID']; ?> </td> 
        <td><?php echo $row['first_name']; ?> </td>                                         
        <td><?php echo $row['last_name']; ?> </td> 
        <td><?php echo $row['company_name']; ?> </td>
        <td><?php echo $sign_in; ?> </td>
        <td><?php echo $sign_out; ?> </td>
        <td><?php echo $sign_in2; ?> </td>
        <td><?php echo $sign_out2; ?> </td>
        <td><?php echo $sign_in3; ?> </td>
        <td><?php echo $sign_out3; ?> </td>        
        <td><?php echo $sign_in4; ?> </td>
        <td><?php echo $sign_out4; ?> </td>        
<!--        <td><?php //echo $row['login_number']; ?> </td> -->    
<!--        <td><?php// echo $hours . $minutes ?> </td> -->
<!--        <td><?php echo $row['is_signin']; ?> </td> -->
<!--        <td><?php echo $row['ip_address']; ?> </td> -->
<!--        <td><?php echo $row['signout_ip'];?> </td> -->
        <td><?php echo $row['date']; ?> </td>                                         
    </tr>

<?php } ?>    

    
   
</table>                              
</div>




        <!-- /.row -->

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; Check In 2015</p>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </footer>
        </div>
    </div>
    

    <!-- /.container -->
    <!-- Bootstrap Core JavaScript -->
    <script src="../js/bootstrap.min.js"></script>
    
</body>

</html>
